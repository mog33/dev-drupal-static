const { src, dest, series, parallel, task } = require("gulp")
const replace = require('gulp-replace')
const gulpCopy = require('gulp-copy')

const del = require("del")
const dom = require("gulp-dom")
const htmlBeautify = require("gulp-html-beautify")
const removeHtml = require("gulp-html-remove")
const uglify = require("gulp-uglify")
const minify = require("gulp-minify")

const COMPILE_FOLDER = "./build"
const SOURCE_FOLDER = "./src"

const cleanBuild = () => del(["build/**"])

const clean = () =>
  del([
    "src/**",
    "!src",
    "!src/sites",
    "!src/sites/default",
    "!src/sites/default/files",
    "!src/sites/default/files/styles/",
    "src/sites/default/files/styles/media_library",
    "src/sites/default/files/css/**",
    "src/sites/default/files/js/**",
  ])

const cleanBuildCss = () => del(["build/sites/default/files/css/**"])

const cleanBuildJs = () => del(["build/sites/default/files/js/**"])

const htmlBeautifyOptions = {
  indent_size: 2,
  preserve_newlines: false,
  wrap_attributes_indent_size: 0,
}

const removeHtmlAttributes = [
  // 'data-drupal-selector',
  "data-drupal-settingstray",
  "data-drupal-link-system-path",
  "data-off-canvas-main-canvas",
  "data-drupal-link-query",
  "data-drupal-messages-fallback",
]

const cleanHtml = () =>
  src(`${SOURCE_FOLDER}/**/*.html`)
    .pipe(
      dom(function () {
        const discardAttributes = (element, attributes) =>
          attributes.forEach((attribute) => element.removeAttribute(attribute))
        return this.querySelectorAll("header, button, div, nav, ul, li, a").forEach((elem) => {
          discardAttributes(elem, removeHtmlAttributes)
        })
      })
    )
    // .pipe(removeHtml('[rel="preload"]'))
    .pipe(htmlBeautify(htmlBeautifyOptions))
    .pipe(dest(`${COMPILE_FOLDER}/`))

const assets = () => src(`${SOURCE_FOLDER}/**/*.{png,gif,jpg,webp,ico,svg,xml,css}`).pipe(dest(`${COMPILE_FOLDER}/`))

const assetsJs = () =>
  src(`${SOURCE_FOLDER}/**/*.js`)
    .pipe(uglify())
    // .pipe(minify({
    //   noSource: true,
    //   ext:{
    //       min:'.js'
    //   },
    // }))
    .pipe(dest(`${COMPILE_FOLDER}/`))

const fixFontsPathCss = () =>
  src(`${SOURCE_FOLDER}/**/*.css`)
    .pipe(replace('/themes/custom/ddev_theme/fonts/', '/fonts/'))
    .pipe(dest(`${COMPILE_FOLDER}/`))

const fixFontsPath = () =>
  src(`${COMPILE_FOLDER}/**/*.html`)
    .pipe(replace('/core/themes/olivero/fonts/', '/fonts/'))
    .pipe(dest(`${COMPILE_FOLDER}/`))

const cpFonts = () =>
  src(`${SOURCE_FOLDER}/themes/custom/ddev_theme/fonts/**/*.*`)
    .pipe(dest(`${COMPILE_FOLDER}/fonts/`))

task("clean", parallel(clean, cleanBuild))
task("cleanHtml", cleanHtml)
task("assets", series(parallel(cleanBuildCss, cleanBuildJs), parallel(assets, assetsJs)))
task("assetsJs", series(cleanBuildJs, assetsJs))

task("build", series(cleanBuild, parallel(cleanHtml, assets, assetsJs), fixFontsPathCss, fixFontsPath, cpFonts))
