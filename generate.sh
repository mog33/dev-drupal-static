#!/usr/bin/env bash

echo -e "Generate static..."

cd ../dev-drupal/

# ddev exec drush -y cset system.performance css.preprocess true
# ddev exec drush -y cset system.performance js.preprocess true
# ddev exec drush -y cset system.performance cache.page.max_age 86400

# mv ./web/sites/default/settings.local.php ./web/sites/default/settings.local.php.dis

# ddev exec drush -y cr

echo "Tome drush failing, do from web interface: https://dev-drupal.ddev.site/admin/config/tome/static/generate"

# ddev exec drush -y tome:static --uri=https://developpeur-drupal.com --ignore-warnings --process-count=1 --path-count=1 --retry-count=0

echo -e "Copy fonts..."

rm -rf ../dev-drupal-static/src/fonts/ ../dev-drupal-static/src/core/themes/olivero/fonts/
cp -r ./web/core/themes/olivero/fonts/ ../dev-drupal-static/src/
cp -r ./web/core/themes/olivero/fonts/ ../dev-drupal-static/src/core/themes/olivero/

echo -e "Copy files..."

# if [ -d ../dev-drupal-static/src.bak/ ]; then mv ../dev-drupal-static/src.bak/ ../dev-drupal-static/src.bak.bak/; fi
# mv ../dev-drupal-static/src/ ../dev-drupal-static/src.bak/
mkdir -p ../dev-drupal-static/src/
cp -r ./html/* ../dev-drupal-static/src/

# mv ./web/sites/default/settings.local.php.dis ./web/sites/default/settings.local.php

echo -e "Done!"